class Trip < ActiveRecord::Base
  
   
  def self.find_by_date_range start,fn
    where("arrive_date BETWEEN ? AND ?", start, fn)
  end

end
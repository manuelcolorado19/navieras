class CreateDetails < ActiveRecord::Migration[5.2]
  def change
    create_table :details do |t|
      t.references :containers, foreign_key: true 
      t.references :masters, foreign_key: true
      t.references :nvoccs, foreign_key: true
      end
  end
end

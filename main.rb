require_relative 'lib/library'

class Main 

  def self.find_bls(shipping_company)
      Master.find_bls(shipping_company).each do |new|
      puts "codigo del bl asociado a la naviera #{new.code}"
    end
  end

  def self.find_containers kind 
    puts "contenedores del tipo #{kind}" 
    Container.list_containers_by_type(kind).each do |container|
      puts "codigo del container #{container.code}"
    end
  end

  def self.get_top_ten start,fn 
    puts "top 10 paises con más atraques entre #{start} y #{fn}" 
     d=Trip.find_by_date_range(start,fn).group(:destiny).count
     d.sort.reverse.each do |country|
       puts Country.find(country[0]).name
     end
  end
   def self.get_complete_bl bl_code
     union=Master.joins(:company)
     bl= union.find(bl_code)
     houses_of_bl=House.where('masters_id = ?', bl_code)
     containers=Details.where('masters_id = ?', bl_code)
     trips = Master.joins(:trip).find(bl_code)
     origin = Country.find(trips.trip.origin).name
     destiny = Country.find(trips.trip.destiny).name 
     print_bl bl,houses_of_bl,containers,trips,origin, destiny
    end

    def self.print_bl bl,houses_of_bl,containers,trips,origin, destiny
      puts "Nombre de la naviera: #{bl.company.name}"
      puts "lista de Bl houses:"
      houses_of_bl.each do |houses|
        puts houses.code
      end 
      puts "lista de containers:"
      containers.each do |f|
        puts Container.find(f.containers_id).code
      end 
      puts "el origen del viaje fue: #{origin}"
      puts "el destino del viaje fue: #{destiny}"
      puts "Fecha de atraque: #{bl.arrive_date}"
      puts "Codigo del viaje: #{trips.code}"
     end 

end 


Main.find_bls 1
Main.find_containers 2
Main.get_top_ten("2017-01-01", "2018-12-30")
Main.get_complete_bl 1


require_relative 'library'

Shipping_company.destroy_all
Cargo_ship.destroy_all
Container.destroy_all
Country.destroy_all
Trip.destroy_all
Master.destroy_all
Consignee.destroy_all
Nvocc.destroy_all
House.destroy_all
 
Shipping_company.create!([{
  name: "Puerto La Guairita"
}])
 
Trip.create!([{
  code: 123,
  origin: 1,
  destiny: 2,
}
])

Cargo_ship.create!([{
  name: "Titanic",
  trips_id: 1,
  shipping_companies_id: 1
}])

Container.create!([{
  name: "ABCD1234",
  size:1,
  kind:2
}])

Country.create!([{
  name: "VENEZUELA"
},
{
  name: "COLOMBIA"

}])


Master.create!([{
  code: 123456,
  shipping_companies_id: 1,
  arrive_date: 10.days.ago,
  trips_id:1

}
])

Consignee.create!([{
  name: 'Manuel Colorado',

}
])

Nvocc.create!([{
  name: 'Liberty Express',

}
])

House.create!([{
  code: 54321,
  NVOCCS_id: 1,
  consignees_id:1,
  masters_id:1
}
])


House.create!([{
  code: 54321,
  NVOCCS_id: 1,
  consignees_id:1,
  masters_id:1
}
])

Details.create!([{
  code: 54321,
  NVOCCS_id: 1,
  consignees_id:1,
  masters_id:1
}
])
p "Migrated"


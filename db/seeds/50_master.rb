Master.create!([{
  code: 123,
  companies_id: 1,
  arrive_date: 10.days.ago,
  trips_id:1
},
{
  code: 456,
  companies_id: 1,
  arrive_date: 9.days.ago,
  trips_id:2
},
{
  code: 789,
  companies_id: 1,
  arrive_date: 8.days.ago,
  trips_id:3
},
{
  code: 2123,
  companies_id: 1,
  arrive_date: 7.days.ago,
  trips_id:4
}
])
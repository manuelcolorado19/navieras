class CreateMasters < ActiveRecord::Migration[5.2]
  def change
    create_table :masters do |t|
      t.integer :code
      t.references :companies, foreign_key: true
      t.date :arrive_date
      t.references :trips, foreign_key: true
    end
  end
end

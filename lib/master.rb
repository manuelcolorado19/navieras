class Master < ActiveRecord::Base
  validates :code, :presence => true
  validates :companies_id, :presence => true
  validates :arrive_date, :presence => true
  validates :trips_id, :presence => true

  has_many :houses
  belongs_to :company, foreign_key: 'companies_id'
  belongs_to :trip, foreign_key: 'trips_id'

  def self.find_bls(id)
    g=where("companies_id = ? ", id)
    get_rescue
    g
  end
    
  def self.get_rescue
    rescue ActiveRecord::RecordNotFound => e
    p "registro no econtrado"
  end

end



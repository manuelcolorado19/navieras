# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_08_23_031503) do

  create_table "cargo_ships", force: :cascade do |t|
    t.string "name"
    t.integer "trips_id"
    t.integer "companies_id"
    t.index ["companies_id"], name: "index_cargo_ships_on_companies_id"
    t.index ["trips_id"], name: "index_cargo_ships_on_trips_id"
  end

  create_table "companies", force: :cascade do |t|
    t.string "name"
  end

  create_table "consignees", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "consignees_containers", force: :cascade do |t|
    t.integer "consignees_id"
    t.integer "containers_id"
    t.integer "houses_id"
    t.index ["consignees_id"], name: "index_consignees_containers_on_consignees_id"
    t.index ["containers_id"], name: "index_consignees_containers_on_containers_id"
    t.index ["houses_id"], name: "index_consignees_containers_on_houses_id"
  end

  create_table "containers", force: :cascade do |t|
    t.string "code", null: false
    t.integer "kind", null: false
    t.integer "size", null: false
  end

  create_table "countries", force: :cascade do |t|
    t.string "name"
  end

  create_table "details", force: :cascade do |t|
    t.integer "containers_id"
    t.integer "masters_id"
    t.integer "nvoccs_id"
    t.index ["containers_id"], name: "index_details_on_containers_id"
    t.index ["masters_id"], name: "index_details_on_masters_id"
    t.index ["nvoccs_id"], name: "index_details_on_nvoccs_id"
  end

  create_table "houses", force: :cascade do |t|
    t.integer "code", null: false
    t.integer "NVOCCS_id"
    t.integer "consignees_id"
    t.integer "masters_id"
    t.index ["NVOCCS_id"], name: "index_houses_on_NVOCCS_id"
    t.index ["consignees_id"], name: "index_houses_on_consignees_id"
    t.index ["masters_id"], name: "index_houses_on_masters_id"
  end

  create_table "masters", force: :cascade do |t|
    t.integer "code"
    t.integer "companies_id"
    t.date "arrive_date"
    t.integer "trips_id"
    t.index ["companies_id"], name: "index_masters_on_companies_id"
    t.index ["trips_id"], name: "index_masters_on_trips_id"
  end

  create_table "nvoccs", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "trips", force: :cascade do |t|
    t.integer "code", null: false
    t.integer "origin", null: false
    t.integer "destiny", null: false
    t.date "arrive_date", null: false
    t.integer "container_id"
    t.index ["container_id"], name: "index_trips_on_container_id"
  end

end

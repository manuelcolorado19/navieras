class CreateContainers < ActiveRecord::Migration[5.2]
  def change
    create_table :containers do |t|
      t.string :code, null:false
      t.integer :kind, null:false
      t.integer :size, null:false
    end
  end
end

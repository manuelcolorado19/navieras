class Container < ActiveRecord::Base

  has_many :details
  
  def self.list_containers_by_type kind
    a = where("kind=?", kind)
    get_error a
  end 

  def self.get_error a 
    puts "tipo equivocado" if a == nil 
    a 
  end  
end
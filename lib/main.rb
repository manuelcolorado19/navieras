require 'active_record'
require_relative '../db/connection'
require_relative 'shipping_company'
require_relative 'cargo_ship'
require_relative 'container'
require_relative 'country'
require_relative 'trip'
require_relative 'master'
require_relative 'consignee'
require_relative 'nvocc'
require_relative 'house'
require_relative 'details'



class Main 

    def get_bl_by_shipping (company)
        Master.shipping_company(company)
    end

end 

c= Main.new
puts c.get_bl_by_shipping(1)


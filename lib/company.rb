class Company < ActiveRecord::Base
  validates :name, :presence => true
  has_many :masters


  before_create do |shipping_company|
    Company.name.titleize
  end


end

class CreateConsigneesContainers < ActiveRecord::Migration[5.2]
  def change
    create_table :consignees_containers do |t|
      t.references :containers, foreign_key: true
      t.references :houses, foreign_key: true
    end
  end
end

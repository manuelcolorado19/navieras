class CreateCargoShips < ActiveRecord::Migration[5.2]
  def change
    create_table :cargo_ships do |t|
      t.string :name
      t.references :trips, foreign_key: true
      t.references :companies, foreign_key: true
    end
  end
end

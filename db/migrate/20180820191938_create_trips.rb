class CreateTrips < ActiveRecord::Migration[5.2]
  def change
    create_table :trips do |t|
      t.integer :code, null:false
      t.integer :origin, null:false
      t.integer :destiny, null:false
      t.date :arrive_date, null:false
      t.references :container, foreign_key: true
    end
  end
end

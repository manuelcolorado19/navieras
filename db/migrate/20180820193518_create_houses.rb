class CreateHouses < ActiveRecord::Migration[5.2]
  def change
    create_table :houses do |t|
      t.integer :code, null:false
      t.references :NVOCCS, foreign_key: true 
      t.references :consignees, foreign_key: true
      t.references :masters, foreign_key: true
    end
  end
end

class House < ActiveRecord::Base
    validates :NVOCCS_id, :presence => true
    validates :code, length: { minimum: 3 }
    has_and_belongs_to_many :details
end
letters= ('A'..'Z').to_a
numbers= (0..9).to_a  

20.times do |d|
  Container.create!(
    code: (letters.sample(4)+numbers.sample(3)).join, 
    size: rand(2)+1,
    kind: rand(2)+1
  )   
end
